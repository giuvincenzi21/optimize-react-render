import deepEqual from 'deep-equal';

const defaultProp = {
    shouldComponentUpdate: () => false
};

function optimizeReactRender(shouldComponentUpdate = defaultProp.shouldComponentUpdate) {
    if (typeof shouldComponentUpdate !== 'function') throw new Error(`shouldComponentUpdate must be a function, expected ${typeof shouldComponentUpdate}`);
    const defaultShoulComponentUpdate = shouldComponentUpdate();
    if (typeof defaultShoulComponentUpdate !== 'boolean') throw new Error(`shouldComponentUpdate must return a boolean, expected ${typeof defaultShoulComponentUpdate}`);

    /////
    return function (target) {
        target.prototype.shouldComponentUpdate = function (nextProps, nextState) {
            if (!deepEqual(this.props, nextProps) || !deepEqual(this.state, nextState)) return true;

            if (!defaultShoulComponentUpdate) console.log('BLOCK_CONTAINER_UPDATE', target.name);
            return defaultShoulComponentUpdate;
        };
    };
}

export default optimizeReactRender;